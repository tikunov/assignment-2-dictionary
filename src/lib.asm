section .text

%define DECLARE global
%include "lib.inc"

; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, 60             ; sys_exit
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rsi, rsi
    mov rax, rdi
    .iterate:                       ; over string
        mov sil, [rax]              ; get current char code
        test sil, sil               ; check if now at null terminator
        jz .stop            		; stop if string ended
        inc rax             		; otherwise increase rax by 1
        jmp .iterate        		; and keep iterating
    .stop:
        sub rax, rdi
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi            ; save string pointer
    call string_length  ; get string length
    mov rdx, rax        ; rdx = string length
    pop rsi             ; rsi = string pointer
    mov rdi, 1          ; rdi = file descriptor
    mov rax, 1          ; rax = sys_write signal number
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi            ; save to stack
    mov rsi, rsp        ; rsi = string pointer
    mov rdx, 1          ; rdx = string length
    mov rdi, 1          ; rdi = file descriptor
    mov rax, 1          ; sys_write
    syscall
    pop rdi             ; restore stack
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 10         ; newline char code
    call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    dec rsp
    mov byte[rsp], 0

    xor rcx, rcx
    mov r9, 10

    .iterate:
        xor rdx, rdx
        div r9
        add rdx, '0'
        dec rsp
        mov [rsp], dl
        inc rcx

    test rax, rax
    jne .iterate

    mov rdi, rsp    
    push rcx
    call print_string
    pop rcx
    inc rsp
    add rsp, rcx
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0          ; compare with zero
    jge .print          ; if negative
    neg rdi             ; negate it
    push rdi            ; save to stack
    mov rdi, 45         ; rdi = '-'
    call print_char     ; print minus
    pop rdi             ; restore number from stack
    .print:             ; endif
        call print_uint ; print number
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax        ; result = 0
    xor dx, dx          ; chars
    .iterate:
        mov dh, [rdi]   ; get char from first string
        mov dl, [rsi]   ; get char from second string
        cmp dh, dl      ; compare them
        jne .false      ; if equal
        test dh, dh     ;   check if they're null terminator
        jz .true        ;   if they're not
        inc rdi         ;       move on to next pair of chars and iterate again
        inc rsi         ;   else return true
        jmp .iterate    ; else return false
    .true:
        inc rax         ; result = 1
        ret
    .false:
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax        ; rax = sys_read signal number
    push rax            ; push zero to stack
    xor rdi, rdi        ; rdi = file descriptor
    mov rsi, rsp        ; rsi = buffer address (stack top)
    mov rdx, 1          ; rdx = byte count
    syscall             ; rax = number of read bytes
    pop rcx             ; rcx = buffer
    test rax, rax       ; if read zero bytes
    jz .stop            ; return zero
    mov rax, rcx        ; otherwise return rcx
    .stop:
        ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    xor rcx, rcx
    .iterate:
        push rdi
        push rsi
        push rcx
        call read_char
        pop rcx
        pop rsi
        pop rdi
        cmp rax, 0x20
        je .test_leading_whitespace
        cmp rax, 0x9
        je .test_leading_whitespace
        cmp rax, 0xA
        je .test_leading_whitespace
        cmp rax, 0x0
        je .end
        mov [rdi+rcx], rax
        inc rcx
        cmp rcx, rsi
        jl .iterate
        mov rax, 0
        ret

    .test_leading_whitespace:
        test rcx, rcx
        jz .iterate

    .end:
        mov byte[rdi+rcx], 0
        mov rdx, rcx
        mov rax, rdi
        ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rdx, rdx
    xor r10, r10
    mov r11, 10

    .iterate:
        mov r10b, byte[rdi+rdx]

        cmp r10b, 0x30
        jl .return

        cmp r10b, 0x39
        jg .return

        push rdx

        mul r11

        pop rdx

        sub r10b, 0x30

        add rax, r10

        inc rdx
        jmp .iterate

        .return:
            ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    mov r10b, [rdi]

    cmp r10b, 0x2D
    je .negative

    cmp r10b, 0x2B
    je .positive

    cmp r10b, 0x30
    jl .bad_number

    cmp r10b, 0x39
    jg .bad_number

    jmp .no_sign

    .positive:
        inc rdi
        call parse_uint
        inc rdx
        ret

    .no_sign:
        call parse_uint
        ret

    .negative:
        inc rdi
        call parse_uint
        inc rdx
        neg rax
        ret

    .bad_number:
        xor rdx, rdx
        ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    push rbx

    .iterate:
        mov bl, byte[rdi+rax]
        mov byte[rsi+rax], bl
        dec rdx
        js .error
        cmp byte[rdi+rax], 0
        inc rax
        jne .iterate
        jmp .end

        .error:
            mov rax, 0

        .end:
            pop rbx
            ret
