SRCDIR :=src
OBJDIR :=obj
TARGET :=dict
ASMC :=nasm
ASMCFLAGS :=-felf64 -I$(SRCDIR) -g
DEBUGFLAGS :=$(ASMCFLAGS) -g
LINKER :=ld

ASM_SOURCES :=$(wildcard $(SRCDIR)/*.asm)
OBJECTS :=$(patsubst $(SRCDIR)%,$(OBJDIR)%,$(patsubst %.asm,%.o,$(ASM_SOURCES)))

all: $(OBJECTS)
	$(LINKER) $(OBJECTS) -o $(TARGET)
	@echo "Built successfully, to run the program, call ./$(TARGET)"

$(OBJDIR)/%.o: $(SRCDIR)/%.asm
	@-mkdir $(OBJDIR) 2> /dev/null || true;
	$(ASMC) $(ASMCFLAGS) $< -o $@

clean:
	rm -rf $(OBJDIR)/*

.PHONY: all clean
